/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author Hi! Le Minh Hieu
 */
public class Pay {
    private int payId;
    private Customer customer;
    private Service service;
    private Test test;
    private int payTotal;
    public Pay() {
    }

    public Pay(int payId, Customer customer, Service service, Test test, int payTotal) {
        this.payId = payId;
        this.customer = customer;
        this.service = service;
        this.test = test;
        this.payTotal = payTotal;
    }

    public int getPayId() {
        return payId;
    }

    public void setPayId(int payId) {
        this.payId = payId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public int getPayTotal() {
        return payTotal;
    }
    public void setPayTotal(int payTotal) {
        this.payTotal = payTotal;
    }

    @Override
    public String toString() {
        return "Pay{" + "payId=" + payId + ", customer=" + customer + ", service=" + service + ", test=" + test + ", payTotal=" + payTotal + '}';
    }
    
    
}
