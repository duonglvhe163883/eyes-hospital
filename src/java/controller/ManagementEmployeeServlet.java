package controller;

import dao.EmployeeDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import model.Account;
import model.Customer;
import model.Employee;

public class ManagementEmployeeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        EmployeeDAO dao = new EmployeeDAO();
         String index = request.getParameter("index");
        int index1 = Integer.parseInt(index);
        int count = dao.countEmployee();
        int endPage = count/10;
        if(count%10!=0){
            endPage++;
        }
        List<Employee> listEmployee = dao.pageEmployee(index1);
        request.setAttribute("listEmployee", listEmployee);
        request.getRequestDispatcher("ManagementEmployee.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

}
