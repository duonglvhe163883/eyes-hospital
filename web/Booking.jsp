<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Booking Page</title>
        <link type="text/css" rel="stylesheet" href="style/bootstrap.min.css" />
        <link type="text/css" rel="stylesheet" href="style/booking.css" />
        <link rel="stylesheet" href="style/booking.css"/>

    </head>
    <jsp:include page="Header.jsp"/>
    <body>
        <div id="booking" class="section" style="background-color: #009933">
            <div class="section-center">
                <div class="container">
                    <div class="row">
                        <div class="booking-form">
                            <div class="form-header">
                                <h1 style="color: greenyellow">BOOKING SCHEDULE</h1>
                            </div>
                            <form method="post" action="booking">
                                <div class="row">
                                    <input type="hidden" name="bid" value="${requestScope.customer.customerId}"> 
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            
                                            <span class="form-label">Name</span>
                                            <a class="form-control">${requestScope.customer.customerName} </a>
                                        </div>
                                       
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <span class="form-label">Gender</span>
                                            <a class="form-control">${requestScope.customer.customerGender} </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="form-label">Phone Number</span>
                                    <a class="form-control">${requestScope.customer.customerPhone} </a>
                                </div>
                                <div class="form-group">
                                    <span class="form-label">Address</span>
                                    <a class="form-control">${requestScope.customer.customerAddress} </a>
                                </div>                           
                                <div class="form-group">
                                    <span class="form-label">Reason/Symptom</span>
                                    <input name="reason" class="form-control" type="text" placeholder="Enter Reason/Symptom">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <span class="form-label">Pickup Date</span>
                                            <input name="date" class="form-control" type="date" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-btn">
                                    <button class="submit-btn">Book Now</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<jsp:include page="Footer.jsp"/>
</html>
