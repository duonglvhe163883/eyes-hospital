<%-- 
    Document   : ManageCustomer
    Created on : Feb 14, 2023, 12:11:43 AM
    Author     : Hi! Le Minh Hieu
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="style/managementcustomer.css">
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
      <body>
        <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Bootstrap CRUD Data Table for Database with Modal Form</title>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
            <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <body>
            <div class="container">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2>Register <b>Patient</b></h2>
                            </div>
                            <div class="col-sm-6">
                                <a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add New Service</span></a>						
                            </div>
                        </div>
                    </div>
                    <form action="search_servlet" method="post">
                        <input type="hidden" name="method" value="searchCustomer">
                        <input value="${key}" type="search" placeholder="Search" aria-label="Search" name="keyword"/>
                        <button class="btn btn-outline-success" type="submit">
                            Search
                        </button>
                    </form>
                    <table class="table table-striped table-hover">
                        <thead>
                     
                            <tr>
                                <th>

                                </th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Birth</th>
                                <th>Phone</th>
                                <th>Gender</th>
                                <th></th>
                               
                                
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="i" items="${requestScope.listCusBook}" >
                                    <tr>
                                    <td>

                                    </td>
                                    <td><c:out value = "${i.customerName}"/></td>
                                    <td><c:out value = "${i.customerAddress}"/></td>
                                    <td><c:out value = "${i.customerBirth}"/></td>
                                    <td><c:out value = "${i.customerPhone}"/></td>
                                    <td><c:out value = "${i.customerGender}"/></td>
                                    <td></td>
                                    <td>
                                            <a href="check_patient?cid=${i.customerId}" onclick="return confirm('Are you sure?')" class="done" data-toggle="modal"><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </td>
                                    <td></td>                                    
                                </tr>
                                
                                </c:forEach>
                                <c:forEach var="i" items="${requestScope.listCusRe}" >
                                    <tr>
                                    <td>

                                    </td>
                                    <td><c:out value = "${i.customerName}"/></td>
                                    <td><c:out value = "${i.customerAddress}"/></td>
                                    <td><c:out value = "${i.customerBirth}"/></td>
                                    <td><c:out value = "${i.customerPhone}"/></td>
                                    <td><c:out value = "${i.customerGender}"/></td>
                                    <td></td>
                                   <td>
                                        
                                            <a href="check_patient?cid=${i.customerId}" onclick="return confirm('Are you sure?')" class="done" data-toggle="modal"><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </td>
                                    <td></td>                                    
                                </tr>
                                
                                </c:forEach>
                                
                            </tbody>
                    </table>
                    <div class="clearfix">
                        <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
                        <ul class="pagination">
                            <li class="page-item disabled"><a href="#">Previous</a></li>
                            <li class="page-item"><a href="#" class="page-link">1</a></li>
                            <li class="page-item"><a href="#" class="page-link">2</a></li>
                            <li class="page-item active"><a href="#" class="page-link">3</a></li>
                            <li class="page-item"><a href="#" class="page-link">4</a></li>
                            <li class="page-item"><a href="#" class="page-link">5</a></li>
                            <li class="page-item"><a href="#" class="page-link">Next</a></li>
                        </ul>
                    </div>
                </div>
                        <div id="addEmployeeModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="register_patient_servlet" method="post">
                            <div class="modal-body">
                                 <b>User Name: </b><input type="text" class="form-control" value="" required name="username"><br>
                                 <b>Password: </b><input type="password" class="form-control" value="" required name="password"><br>
                                 <br>
                                 <b>Name: </b><input type="text" class="form-control" value="" required name="name"><br>
                                <b>Address: </b><input type="text" class="form-control" value="" required name="address"><br>
                                <b>Birth: </b><input type="date" class="form-control" required value="" name="birth"><br>
                                <b>Phone:</b><input type="text" class="form-control" required  value="" name="phone"><br>
                                <div>
                                    <label for="Gender"><b>Gender</b> </label>
                                    <input type="radio" name="gender" value="1" /> Male
                                    <input type="radio" name="gender" value="0" /> Female
                                </div>
                                <br>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-success" value="submit">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
            
        </body>
</html>
