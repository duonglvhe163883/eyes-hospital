<%-- 
    Document   : Profile
    Created on : Feb 6, 2023, 9:36:10 PM
    Author     : Hi! Le Minh Hieu
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <script src="js/profile.js"></script>
    <link rel="stylesheet" href="style/profile.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <head>
        <title>Profile</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>

    <jsp:include page="Header.jsp"/>
    <hr>
    <div class="row">

        <div class="col-sm-10"><h1>CUSTOMER INFORMATION :</h1> </div>


    </div>

    <div class="row">
        <div class="col-sm-3"><!--left col-->


            <div class="text-center">
                <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle img-thumbnail" alt="avatar">
                <br><!-- comment -->
                <br>
                <br><!-- comment -->

            </div></hr><br>         
        </div><!--/col-3-->
        <div class="col-sm-9">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
                <li><a data-toggle="tab" href="#messages">Menu 1</a></li>
                <li><a data-toggle="tab" href="#settings">Menu 2</a></li>
            </ul>


            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <hr>


                    <form class="form" action="profile" method="post" id="registrationForm">
                        <c:if test="${sessionScope.account.accountId!=requestScope.customer.accountcId}">
                            <div class="form-group">
                                <div class="col-xs-6">
                                    <label for="Name"><h4> Name</h4></label>
                                    <input type="text" class="form-control" name="namecustomer" id="CustomerEmail" placeholder=" Name" title="enter your  name if any.">
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-xs-6">
                                    <label for="Address"><h4>Address</h4></label>
                                    <input type="text" class="form-control" name="addresscustomer" id="CustomerEmail" placeholder="Address" title="enter your address if any.">
                                </div>
                            </div>

                            <div class="form-group">

                                <div class="col-xs-6">
                                    <label for="Phone"><h4>Phone</h4></label>
                                    <input type="text" class="form-control" name="phonecustomer" id="CustomerEmail" placeholder="Phone" title="enter your phone number if any.">
                                </div>
                            </div>

                            <div class="form-group">

                                <div class="col-xs-6">
                                    <label for="Birth"><h4>Birth:</h4></label>
                                    <input type="date" class="form-control" name="birthcustomer" id="location" placeholder="somewhere" title="">
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-xs-6">
                                    <label for="Gender"><h4>Gender :</h4></label>
                                    <input type="radio" name="customergender" value="1" /> Male
                                    <input type="radio" name="customergender" value="0" /> Female
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <br>
                                    <button name ="check" value ="save" class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                                  
                                </div>
                            </div>
                            
                        </c:if>
                        <hr>
                        <c:if test="${sessionScope.account.accountId==requestScope.customer.accountcId}">

                            <div class="form-group">

                                <div class="col-xs-6">
                                    <label for="Name"><h4>Name: </h4></label>
                                    <a style="font-size: 20px; color: red">${requestScope.customer.customerName} </a>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-xs-6">
                                    <label for="Address"><h4>Address: </h4></label>
                                    <a style="font-size: 20px; color: red">${requestScope.customer.customerAddress}</a>

                                </div>
                            </div>

                            <div class="form-group">

                                <div class="col-xs-6">
                                    <label for="Phone"><h4>Phone: </h4></label>
                                    <a style="font-size: 20px; color: red">${requestScope.customer.customerPhone}</a>
                                </div>
                            </div>

                            <div class="form-group">

                                <div class="col-xs-6">
                                    <label for="Birth"><h4>Birth:</h4></label>
                                    <a style="font-size: 20px; color: red">${requestScope.customer.customerBirth} </a>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-xs-6">
                                    <label for="Gender"><h4>Gender: </h4></label>
                                    <a style="font-size: 20px; color: red">${requestScope.customer.customerGender} </a> 
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <br>
                                    
                                    <button>  <a style ="font-size: 30px " data-target="#editEmployeeModal${sessionScope.account.accountId}" href="#editEmployeeModal?id=${sessionScope.account.accountId}"class="edit" data-toggle="modal">  Edit</a></button>
                                </div>
                            </div>

                        </c:if>
                    </form>

                  <div class="modal fade" id="editEmployeeModal${sessionScope.account.accountId}" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            
                                        </div>     
                                        <form action="edit_profile_servlet" method="post">
                                            <div class="modal-body">
                                                <br>
                                                <br>
                                                <br>
                                                <h4 class="modal-title">Edit Profile</h4>
                                                <b></b><input type="hidden" class="form-control" name="id" value="<c:out value = "${requestScope.customer.accountcId}"/>" readonly=""><br>
                                                <b>Name: </b><input type="text" class="form-control" name="namecustomer" value="<c:out value = "${requestScope.customer.customerName}"/>"><br>
                                                <b>Address: </b><input type="text" class="form-control" name="addresscustomer" value="<c:out value = "${requestScope.customer.customerAddress}"/>"><br>


                                                <b>Phone </b><input type="text" class="form-control"  name="phonecustomer" value="<c:out value = "${requestScope.customer.customerPhone}"/>"><br>
                                                <b>Birth:</b><input type="date" class="form-control"  name="birthcustomer" value="<c:out value = "${requestScope.customer.customerBirth}"/>"><br>
                                                <b>Gender: </b>
                                                    <input type="radio" name="customergender" value="1" checked /> Male
                                                    <input type="radio" name="customergender" value="0" /> Female                                                 
                                                <br>
                                                <br>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-success" value="submit">Submit</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>



                    <hr>

                </div><!--/tab-pane-->

                <div class="tab-pane" id="messages">

                    <h2></h2>

                    <hr>
                    <form class="form" action="##" method="post" id="registrationForm">
                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="Ket Qua"><h4>Ket Qua</h4></label>
                                <input Style="height: 300px;"  type="text" class="form-control" name=ketquacustomer" id="Customeremail" placeholder="" title="">
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="Don Thuoc"><h4>Don Thuoc</h4></label>
                                <input Style="height: 300px;" type="text" class="form-control" name="donthuoccustomer" id="Customeremail" placeholder="" title="">
                            </div>
                        </div>
                    </form>

                </div>
                <div class="tab-pane" id="settings">


                    <hr>
                    <form class="form" action="##" method="post" id="registrationForm">
                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="Booking"><h4>Booking</h4></label>
                                <div class="form-group">
                                    <div class="col-xs-12">

                                        <br>
                                        <label for="Name"><h4>Reason/Symptom: <a style="font-size: 20px; color: red">${requestScope.booking.booking_reason} </a></h4>
                                        </label>
                                        <br>
                                        <label for="Name"><h4>Date: <a style="font-size: 20px; color: red">${requestScope.booking.booking_date} </a></h4>
                                        </label>
                                        <br>
                                        <label for="Name"><h4>Status: <a style="font-size: 20px; color: red">${requestScope.booking.booking_status} </a></h4>
                                        </label>
                                    </div>
                                </div>
                                <a Style="height: 300px;" type="text" class="form-control" name="bookingcustomer" id="Customeremail" placeholder="" title="">
                                </a>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="Hoa Don"><h4>Hoa Don</h4></label>
                                <input Style="height: 300px;" type="text" class="form-control" name="hoadoncustomer" id="CustomerEmail" placeholder="" title="">
                            </div>
                        </div>
                    </form>
                </div>

            </div><!--/tab-pane-->
        </div><!--/tab-content-->

    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>

<jsp:include page="Footer.jsp"/>
</html>
